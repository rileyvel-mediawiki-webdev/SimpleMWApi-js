// Just some custom error types

export class ArticleNotFoundError extends Error {}
export class ReadFailureError extends Error {}
export class EditFailureError extends Error {}
export class SearchFailureError extends Error {}
