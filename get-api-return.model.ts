interface ExistantWikiPage {
    pageid: number;
    ns: number;
    missing: undefined;
    title: string;
    revisions: WikiRevision[];
}

interface NonExistantWikiPage {
    ns: number;
    title: string;
    missing: true;
}

type WikiPage = ExistantWikiPage | NonExistantWikiPage;

interface WikiRevision {
    slots: {
        main: {
            contentmodel: string;
            contentformat: string;
            content: string;
        };
    };
}

export interface GetApiReturn {
    query: {
        pages: WikiPage[];
    };
}
