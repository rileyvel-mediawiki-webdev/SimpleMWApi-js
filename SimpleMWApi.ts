/**
 * SimpleMWApi.js
 * A simple library that provides a shim for easier access to MediaWiki API from browsers.
 *
 * @author  rileyvel
 * @version 6
 * @since   2020-11-11
 * @license CC-0
 *
 */
import {ArticleNotFoundError, EditFailureError, ReadFailureError, SearchFailureError} from "./Errors";
import {GetApiReturn} from "./get-api-return.model";

export class SimpleMWApi {

    private apiInterface: mw.Api;

    constructor() {
        this.apiInterface = new mw.Api();
    }

    /**
     * Gets the unparsed (wikitext) content of a given page
     */
    async readPage(title: string): Promise<string> {
        const returnValue = await this.apiInterface.get(this.getApiWikitextParam(title)) as GetApiReturn;
        const page = returnValue.query.pages[0];
        if (page.missing) {
            throw new ArticleNotFoundError();
        }
        return page.revisions[0].slots.main.content;
    }


    /**
     * Read a JSON page and parse it as JSON.
     * If the page is not JSON, it will throw an error.
     * @param pageName - the name of the page to read
     */
    async readJSON(pageName: string): Promise<unknown> {

        const returnValue = await this.apiInterface.get(this.getApiWikitextParam(pageName)) as GetApiReturn;
        const page = returnValue.query.pages[0];
        if (page.missing) {
            throw new ArticleNotFoundError();
        }

        const revision = page.revisions[0].slots.main;
        if (revision.contentformat !== 'application/json') {
            throw new ReadFailureError(`Page ${pageName} is not JSON`);
        }

        return JSON.parse(revision.content);

    }

    /**
     * Writes wikitext content to a page. Will overwrite existing data.
     * If the page doesn't exist yet, this will create the page.
     */
    async writePage(title: string, content: string, summary = ""): Promise<void> {

        const reqParams = {
            "action": "edit",
            "format": "json",
            "title": title,
            "text": content,
            "summary": summary,
        };

        try {
            await this.apiInterface.postWithEditToken(reqParams);
        } catch (e: any) {
            // wrap errors in SMAPIError for more flexibility
            throw new EditFailureError("Edit failed: " + e.toString());
        }

    }

    /**
     * Checks whether a page exists
     * @param title - title to check
     */
    async pageExists(title: string): Promise<boolean> {

        try {
            await this.readPage(title);
        } catch (e) {
            if (e instanceof ArticleNotFoundError) {
                return false;
            } else {
                throw e;
            }
        }

        return true;

    }

    private static getApiPath(): string {
        return mw.config.get('wgScript').replace('index', 'api');
    }

    /**
     * Perform a search of the entire wiki
     * By default this returns the maximum number of titles that the API allows.
     * To get even more, run another search with the offset approach.
     * @param term - the term to search for
     * @param offset - if you're performing multiple searches to find all pages matching a query, use this to continue.
     * @param namespace - which name space to search in. Defaults to all namespaces.
     * For more details, see https://www.mediawiki.org/wiki/API:Search#:~:text=srnamespace,within%20these%20namespaces.
     * @returns - a list of strings that are titles of pages matching the search
     */
    async search(term: string, offset: number = 0, namespace: string = '*'): Promise<string[]> {

        const params: Record<string, string> = {
            "action": "query",
            "format": "json",
            "list": "search",
            "srsearch": term,
            "srlimit": "max",
            "srnamespace": namespace,
            "sroffset": `${offset}`
        };

        const res = await fetch(SimpleMWApi.getApiPath(), {
            method: 'POST',
            body: new URLSearchParams(params)
        });

        if (!res.ok) throw new ReadFailureError(`Server returned status code ${res.status}`);

        const json = await res.json();
        if (json.error) throw new SearchFailureError(JSON.stringify(json.error));

        return json.query.search.map((item: any) => item.title);

    }

    private getApiWikitextParam(title: string) {
        return {
            "action": "query",
            "format": "json",
            "prop": "revisions",
            "titles": title,
            "formatversion": "2",
            "rvprop": "content",
            "rvslots": "main",
            "rvlimit": "1"
        };
    }

}
