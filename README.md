# SimpleMWApi.js

A simplified shim for the mw.Api interface.

# How to Use

Easy as...

```Javascript
const myApi = new SimpleMWApi();
var content = await myApi.readPage(title);
myApi.writePage(title, newContent, summary).then(...);
```

# Purpose

This library isn't intended to replace, or compete with, mw.Api. It's just here to enable gadget or userscript
developers to easily get started with some really basic functionalities.

# Installing as an NPM module

`npm i simple-mw-api`

# Licence

Public domain (CC-0)

* May you do good and not evil.
* May you find forgiveness for yourself and forgive others.
* May you share freely, never taking more than you give.
